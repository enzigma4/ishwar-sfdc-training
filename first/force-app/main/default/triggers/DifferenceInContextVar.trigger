trigger DifferenceInContextVar on Job__c (before insert, before update, before delete, after undelete, after insert, after update, after delete) {
    
    if(Trigger.isInsert){
        if(Trigger.isBefore){            
            system.debug('***BEFORE INSERT*** Trigger.New => '+ Trigger.New);
            system.debug('***BEFORE INSERT*** Trigger.NewMap => '+ Trigger.NewMap);
            system.debug('***BEFORE INSERT*** Trigger.Old => '+ Trigger.Old);
            system.debug('***BEFORE INSERT*** Trigger.OldMap => '+ Trigger.OldMap);
        }else if(Trigger.isAfter){          
           system.debug('***AFTER INSERT*** Trigger.New => '+ Trigger.New);
            system.debug('***AFTER INSERT*** Trigger.NewMap => '+ Trigger.NewMap);
            system.debug('***AFTER INSERT*** Trigger.Old => '+ Trigger.Old);
            system.debug('***AFTER INSERT*** Trigger.OldMap => '+ Trigger.OldMap);
        }
    }
    
    if(Trigger.isUpdate){
        if(Trigger.isBefore){           
            system.debug('***BEFORE UPDATE*** Trigger.New => '+ Trigger.New);
            system.debug('***BEFORE UPDATE*** Trigger.NewMap => '+ Trigger.NewMap);
            system.debug('***BEFORE UPDATE*** Trigger.Old => '+ Trigger.Old);
            system.debug('***BEFORE UPDATE*** Trigger.OldMap => '+ Trigger.OldMap);
        }else if(Trigger.isAfter){           
           system.debug('***AFTER UPDATE*** Trigger.New => '+ Trigger.New);
            system.debug('***AFTER UPDATE*** Trigger.NewMap => '+ Trigger.NewMap);
            system.debug('***AFTER UPDATE*** Trigger.Old => '+ Trigger.Old);
            system.debug('***AFTER UPDATE*** Trigger.OldMap => '+ Trigger.OldMap);
        }
    }
    
    if(Trigger.isDelete){
        if(Trigger.isBefore){ 
            system.debug('***BEFORE DELETE*** Trigger.New => '+ Trigger.New);
            system.debug('***BEFORE DELETE*** Trigger.NewMap => '+ Trigger.NewMap);
            system.debug('***BEFORE DELETE*** Trigger.Old => '+ Trigger.Old);
            system.debug('***BEFORE DELETE*** Trigger.OldMap => '+ Trigger.OldMap);
        }else if(Trigger.isAfter){           
           system.debug('***AFTER DELETE*** Trigger.New => '+ Trigger.New);
            system.debug('***AFTER DELETE*** Trigger.NewMap => '+ Trigger.NewMap);
            system.debug('***AFTER DELETE*** Trigger.Old => '+ Trigger.Old);
            system.debug('***AFTER DELETE*** Trigger.OldMap => '+ Trigger.OldMap);
        }
    }
    if(Trigger.isUndelete){
        if(Trigger.isAfter){ 
            system.debug('***AFTER UNDELETE*** Trigger.New => '+ Trigger.New);
            system.debug('***AFTER UNDELETE*** Trigger.NewMap => '+ Trigger.NewMap);
            system.debug('***AFTER UNDELETE*** Trigger.Old => '+ Trigger.Old);
            system.debug('***AFTER UNDELETE*** Trigger.OldMap => '+ Trigger.OldMap);
        }
    }
}