trigger UpdateJob on Job__c (before update, after delete) {  
    
    if(trigger.isUpdate){
        JobHandler.getData(Trigger.New);
        JobHandler.sentMail(Trigger.New);
    }
    if(trigger.isDelete){
        JobHandler.jobStatus(Trigger.old);
    }
}