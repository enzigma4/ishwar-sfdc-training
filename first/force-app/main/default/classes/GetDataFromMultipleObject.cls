public class GetDataFromMultipleObject {

public static List<List<String>> getObjectList(){
    List<List<String>> strList = new List<List<String>>();
    List<Account> acc = [select name,phone from account limit 2];

    for(account a : acc){
        List<String> tempList = new List<String>();
        tempList.add('account');
        tempList.add(a.name);
        tempList.add(a.phone);
        strList.add(tempList);
    }
    List<contact> cList = [select name,phone from contact limit 2];
    for(contact a : cList){
        List<String> tempList = new List<String>();
        tempList.add('contact');
        tempList.add(a.name);
        tempList.add(a.phone);
        strList.add(tempList);
    }
    return strList;
} 
}