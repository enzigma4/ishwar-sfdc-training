@isTest
public class TestUpdateCandidate {
    
    @testSetup static void methodName() {
        Contact con = new Contact(LastName = 'ishwar shinde');
        insert con;
        List<Contact> contactId = [SELECT id FROM Contact WHERE LastName=:'ishwar shinde' LIMIT 1];
        
        Job__c jobdata = new Job__c();
        jobdata.Name = 'a035g0000017V6A';
        jobdata.Active__c = true;
        jobdata.Number_of_Positions__c = 5;
        jobdata.Salary_Offered__c =1000; 
        
        Candidate__c canddata = new Candidate__c();
        canddata.Name = 'Ishwar';
        canddata.First_Name__c = 'Ishwar';
        canddata.Last_Name__c = 'Shinde';
        canddata.Email__c = 'ishwar@gmail.com';
        canddata.Status__c = 'Hired';
        canddata.Application_Date__c = null;
        canddata.Job__c = jobdata.id;
        //canddata.Application_Date__c=null;
        canddata.Expected_Salary__c =0;
        canddata.Job__c = jobdata.Name;
        canddata.Country__c = 'India';
        canddata.State__c = 'Maharashtra';
        jobdata.Manager_assign__c = contactId[0].id; 
        
        insert canddata;
        insert jobdata;
        
        List<Candidate__c> candBulkList=new List<Candidate__c>();
        List<Job__c> jobBulkList = new  List<Job__c>();
        for(Integer i = 0 ; i<50 ; i++) {
            
            candBulkList.add(new Candidate__c(First_Name__c='Ishwar'+i,Last_Name__c='Pawar'+i,Email__c='ishwar'+i+'@gmail.com',Application_Date__c = null, Expected_Salary__c = 0,Job__c = jobdata.Name, Country__c = 'India',State__c = 'Maharashtra', Status__c = 'Hired'));
            jobBulkList.add(new Job__c(Active__c=true,Number_of_Positions__c=i, Name = 'a035g0000017V6A', Salary_Offered__c =1000+i, Manager_assign__c = contactId[0].id));
        }
        
        insert candBulkList;
        insert jobBulkList;
    }
    
    @isTest public static void testAddDate(){
        Candidate__c cand = [SELECT Application_Date__c FROM Candidate__c WHERE Name='Ishwar' LIMIT 1];    
        Test.startTest(); 
        try{
            cand.Application_Date__c = System.today();
            insert cand;
        }catch(System.Exception e){
            String message = e.getMessage();
        }
        Test.stopTest();
    }
    
    @isTest public static void testCheckActive(){
        Job__c jb = [select Active__c from Job__c where Active__c=:true LIMIT 1];
        Test.startTest();      	
        try{
            jb.Active__c = false;
            update jb;
            system.assert(jb.Active__c == false);
        }catch(DmlException e){
            String message = e.getMessage();
            system.assertEquals('This Job is not active. You can not apply for this job', message);
            //System.assertEquals('This Job is not active. You can not apply for this job' , e.getDmlStatusCode(0) );
        }
        Test.stopTest();
    }
    
    @isTest public static void testCheckSalary(){
        Candidate__c cand = [select Expected_Salary__c from Candidate__c where Expected_Salary__c=:0 LIMIT 1];
        Test.startTest();      	
        try{
            cand.Expected_Salary__c = null;
            update cand;
            if(cand.Expected_Salary__c == null){
                //throw 'An exception should have been thrown by the trigger but was not.';
            }
            
        }catch(Exception e){
            String message = e.getMessage();
            
            //system.assert(message.contains('Please fill salary field'));
            //system.assertEquals('Expected Salary field is missing', message);
            //System.assert(message.contains('Insert failed.'));
            //System.assertEquals('Please fill salary field' , e.getDmlStatusCode(0) );
            if(cand.Expected_Salary__c == null){
            	Boolean expectedExceptionThrown =  e.getMessage().contains('Please fill salary field') ? true : false;
				System.AssertEquals(expectedExceptionThrown, false);
            }
        }
        
        try{
            List<Candidate__c> candList= [SELECT Expected_Salary__c FROM Candidate__c Where Expected_Salary__c=0];
            System.assertEquals(50,candList.size());
        }
        catch(Exception e)
        {
            String message = e.getMessage();
            System.assert(message.contains('Insert failed.'));
            //System.assert(message.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
            system.assert(message.contains('Please fill salary field'));
        }
        Test.stopTest();
    }
}