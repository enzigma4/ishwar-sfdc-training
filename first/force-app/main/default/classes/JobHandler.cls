public class JobHandler {
    
    public static void getData(List<Job__c> jobData){
        for(Job__c data : jobData){
            if(data.Number_of_Positions__c == data.Hired_Applicants__c  && data.Active__c){
                data.Active__c=false;
            }else if (data.Number_of_Positions__c > data.Hired_Applicants__c  && !data.Active__c){
                data.Active__c=true;
            }
        }
    }
    
    public static void sentMail(List<Job__c> joblst){  
        Set<Id> managerIds = new Set<Id>();
        
        for(Job__c job:joblst) { 
            if(job.Number_of_Positions__c == job.Hired_Applicants__c && job.Manager_assign__c != null){
                managerIds.add(job.Manager_assign__c);
            }
        }
        
        Map<Id, Contact> contacts;
        if(!managerIds.isEmpty()){
            contacts = new  Map<Id, Contact>([Select Email,id,Name from Contact
                                              where id IN : managerIds]);
        }
        
        if(contacts == null || contacts.isEmpty()){
            return;
        }
		        
        Contact a, con;
        for (ID idKey : contacts.keyset()) {
            a = contacts.get(idKey);
            
        }
        
        for(Job__c data:joblst){ 
            if(data.Number_of_Positions__c == data.Hired_Applicants__c){
                
                con = contacts.get(a.ID);
                
            }
        }
        
        for(Job__c data:joblst){ 
            if(data.Number_of_Positions__c == data.Hired_Applicants__c){
                
                List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
                List<String> sendTo = new List<String>();
                sendTo.add(con.Email);
                mail.setToAddresses(sendTo);
                
                mail.setSubject('Reached Vaccancy Limit!..');
                String body = 'All required candidate has been hired for this job on '+ data.LastModifiedDate;
                
                mail.setHtmlBody(body);
                mails.add(mail);
                Messaging.sendEmail(mails);      
                
            }
            
        }
    }
    
    public static void jobStatus(List<Job__c> joblst){
        for(Job__c job : joblst){
            if(job.Active__c == true){
            	job.addError('This Job is active and can not be deleted');
        	}
        }
    }
}