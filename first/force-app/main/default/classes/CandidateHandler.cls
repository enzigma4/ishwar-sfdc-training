public class CandidateHandler {
    
    public static void addDate(List<Candidate__c> candlst){
        for(Candidate__c cand : candlst){   
            if(cand.Application_Date__c != System.today() || cand.Application_Date__c==null){
                cand.Application_Date__c = system.today();//correction
            }
        }
    }
    
    public static void checkActive(List<Candidate__c> candlst){
        Map <Id, Job__c> JobActive= new Map<Id, Job__c>([select Active__c from Job__c where Active__c=null]);//correction
        for(Candidate__c data: candlst){
            if(JobActive.get(data.Job__c) != null){
                data.addError('This Job is not active. You can not apply for this job');
            }
        }
    }
    
    public static void checkSalary(List<Candidate__c> candlst){
        for(Candidate__c data: candlst){
            if(data.Expected_Salary__c == null){
                data.addError('Please fill salary field');
            }
        }
    }
}